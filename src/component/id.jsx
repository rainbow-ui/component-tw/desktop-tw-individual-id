import { Component, UISmartPanelGrid, UIText } from "rainbowui-desktop-core";
import { Util } from "rainbow-desktop-tools"
import PropTypes from 'prop-types';
import '../css/component.css';
import {ValidatorContext} from 'rainbow-desktop-cache';
import customerValidator from './customerValidator';

export default class TwIndividualId extends Component {
    constructor(props){
        super(props)
        this.state={
        }
        this.id = "individualId-"+this.generateId()        
    }

    render(){
        let labelNoI18n = this.props.label?false:true;        
        return(
            <div id="desktop-id-card">
                <UIText  id={this.id} label={this.props.label?this.props.label:r18n.idCard} colspan={this.props.colspan} noI18n={labelNoI18n}
                 layout='horizontal' model={this.state} property="IdNo" required={this.state.required?this.state.required:'false'} maxLength="10" 
                 validator={customerValidator} onKeyDown={this.onKeyDown.bind(this)} onBlur={this.onBlur.bind(this)}
                 toUpperLowerCase="upper"/>
            </div>
        )
    }

    onKeyDown() {
        if(this.state.IdNo&&this.state.IdNo.length == 10){
            ValidatorContext.validateField(this.id);
            this.props.model.IdNo = this.state.IdNo;
        }
    }

    onBlur(){
        let upIdNo = this.state.IdNo.toUpperCase();
        this.props.model.IdNo = upIdNo;        
        // this.setState({IdNo:upIdNo})
        if(this.props.onBlur){
            this.props.onBlur()
        }
    }

    generateId(){
        let index = new Date().getTime();        
        return "rainbow-ui-" + (index++);
    }
};

TwIndividualId.propTypes = $.extend({}, Component.propTypes, {
    label: PropTypes.string,
    required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
});


TwIndividualId.defaultProps = $.extend({}, Component.defaultProps, {
    colspan: "1",
    required: false
});
